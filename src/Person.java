public class Person {

    // Attributes
    private String firstName;
    private String surname;
    private Pet myPet;

    // Constructor
    public Person (String firstName, String surname)
    {
        this.firstName = firstName;
        this.surname = surname;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getSurname() {
        return this.surname;
    }

    public String getFullName(){
        return this.firstName + " " + this.surname;
    }

    public Pet getMyPet(){
        return this.myPet;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMyPet(Pet myPet){
        this.myPet = myPet;
    }
}
